//---------------------------------------------
// Unit Test packages
//---------------------------------------------
import React from 'react';
import { configure, shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
// https://airbnb.io/enzyme/docs/api/

//---------------------------------------------
// Local packages 
//---------------------------------------------
import Movie from './Movie';
import MovieSettings from '../services/MovieSettings';
import ContentService from '../services/ContentService';
//---------------------------------------------

describe('Movie Model', () => {
	configure({ adapter: new Adapter() });

	let movie, movieData;

	beforeEach(() => {
        movieData = {
			id:99,
			name:'Movie',
			title:'Terminator 2',
			genre_ids:[1,3],
			popularity:5,
			poster_path:'terminator.jpg'
        };
		movie = new Movie(movieData);
	});
    
    it('init as expected', () => {
        expect(movie).toEqual(movieData);
    }); 

    it('calls renderImage() as expected', () => {
        expect(movie.renderImage()).toEqual(MovieSettings.imageUrl + movie.poster_path);
    }); 

    it('calls renderGenres() as expected', () => {
		ContentService.setGenres([
			{id:1, name:'Drama'},
			{id:2, name:'Action'},
			{id:3, name:'Fantasy'}
		]);
        expect(movie.renderGenres()).toEqual('Drama, Fantasy');
    }); 
    
});