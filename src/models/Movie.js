import MovieSettings from '../services/MovieSettings';
import ContentService from '../services/ContentService';

export default class Movie {

    constructor(movie) {
        Object.assign(this, movie);
    }

    /**
    * Get full image path
    * @return {string} imagePath 
    */    
    renderImage() {
        return MovieSettings.imageUrl + this.poster_path;
    }
    
    /**
    * Get comma separated genres
    * @return {string} genres
    */    
    renderGenres() {
        return this.genre_ids
            //because loaded genres might not match an array of movie genres, we filter only matching...
            .filter(id => ContentService.getGenres().find(genre => genre.id === id))
            //now map id's to genre titles
            .map(id => ContentService.getGenres().find(genre => genre.id === id))
            //return genre name
            .map(genre => genre.name)
            //and combine with comma separator
            .join(', ');
    }
  
};