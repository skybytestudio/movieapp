import React, { Component } from 'react';
import Header from './components/Header';
import Footer from './components/Footer';
import Content from './components/Content';
import './styles/App.css';

class App extends Component {

	render() {
		return (
			<div id="wrapper">
				<Header />
				<div id="content">
					<Content />
				</div>
				<Footer />
			</div>
		);
	}
}

export default App;
