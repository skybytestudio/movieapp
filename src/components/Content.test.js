//---------------------------------------------
// Unit Test packages
//---------------------------------------------
import React from 'react';
import { configure, shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
// https://airbnb.io/enzyme/docs/api/

//---------------------------------------------
// Local packages 
//---------------------------------------------
import Content from './Content';
//---------------------------------------------

describe('Content Component', () => {
	configure({ adapter: new Adapter() });

	const sleep = (ms) => {
		return new Promise(resolve => setTimeout(resolve, ms));
	}

	it('renders as expected', async () => {
		const component = shallow(<Content />)
		jest.setTimeout(30000); //test framework timeout until promises are rejected, setting to high to keep waiting...

		await component.instance().componentDidMount();
		expect(component.state('status')).toEqual('loading');

		await sleep(1000); //this needs to be shorter than jest.setTimeout() value to resolve properly.

		expect(component.state('status')).toEqual('loaded');
		expect(component.state('movies').length).toBeGreaterThan(0);
		expect(component.state('genres').length).toBeGreaterThan(0);
		
		// Demo: Force update to sync component with state
		//component.update()
		//expect(component.text()).toBe('...'); 
	})	
	

});