import React, { Component } from 'react';
import '../../styles/MovieCard.css';

export default class MovieCard extends Component {

    render() {
        return (
            <div className="movie-card">
                <img className="movie-card-image" data-id="image" src={this.props.movie.renderImage()} alt={this.props.movie.title} />
                <div className="movie-card-body">
                    <h4 className="movie-card-title" data-id="title">{this.props.movie.title}</h4>
                    <p className="movie-card-genres" data-id="genres">{this.props.movie.renderGenres()}</p>
                    <p className="movie-card-rating" data-id="rating">popularity {this.props.movie.popularity}</p>
                </div>
            </div>
        );
    }

};
