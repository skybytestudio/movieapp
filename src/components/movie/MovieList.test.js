//---------------------------------------------
// Unit Test packages
//---------------------------------------------
import React from 'react';
import { configure, shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
// https://airbnb.io/enzyme/docs/api/

//---------------------------------------------
// Local packages 
//---------------------------------------------
import MovieList from './MovieList';
import Movie from '../../models/Movie';
import ContentService from '../../services/ContentService';
//---------------------------------------------

describe('MovieList Component', () => {
	configure({ adapter: new Adapter() });

	let props, movies, genres;

	beforeEach(() => {
		ContentService.setGenres([
			{id:1, name:'Drama'},
			{id:2, name:'Action'},
			{id:3, name:'Fantasy'}
        ]);
        ContentService.setRating(0);
		movies = [
            new Movie({
                id:99,
                name:'Movie',
                title:'Terminator 1',
                genre_ids:[1,2,3],
                popularity:0,
                vote_average:0,
                poster_path:'terminator1.jpg'
            }),
            new Movie({
                id:100,
                name:'Movie',
                title:'Terminator 2',
                genre_ids:[1,2],
                popularity:0,
                vote_average:0,
                poster_path:'terminator2.jpg'
            })
        ];
		props = {key:1,movies:movies};
	});
  
	it('renders as expected with unmatched filter settings', () => {
        const component = mount(<MovieList {...props} />);
        const movies = component.instance().filterList();
        expect(movies.length).toEqual(0);
	}); 

	it('renders as expected with rating = 5, then movies should be visible above this rating...', () => {
        ContentService.setRating(5);
        props.movies[0].popularity = 6;
        props.movies[0].vote_average = 6;
        props.movies[1].popularity = 7;
        props.movies[1].vote_average = 7;
        
        const component = mount(<MovieList {...props} />);
        expect(component.instance().filterList().length).toEqual(2);
	}); 
    
    
});