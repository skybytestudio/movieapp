import React, { Component } from 'react';

import MovieCard from './MovieCard';
import ContentService from '../../services/ContentService';
import '../../styles/MovieList.css';

export default class MovieList extends Component {

    /**
    * filter list of Movies based on ContentService filters....
    * @return {Array} movies 
    */    
    filterList(){
        return this.props.movies
            //Filter by rating
            .filter(movie => movie.vote_average > ContentService.getRating())
            //Filter by genre
            .filter(movie => {
                //by default, genreIds is empty, since no checkbox is selected by the user.
                //once selected, we filter only movies by selected Ids
                let genreIds = [];
                ContentService.getGenres().forEach(genre => {
                    if(genre.selected){
                        genreIds.push(genre.id)
                    }
                });
                if(genreIds.length > 0){
                    return movie.genre_ids.filter(id => genreIds.indexOf(id) >= 0).length;
                }else{
                    return true;
                }
            })
            //Sort by popularity ascending
            .sort((a, b) => b.popularity - a.popularity);
    }

    render() {
        //Display a friendly message if filters returned no results...
        if(this.filterList().length === 0){
            return (
                <div className="message">
                    <p>Sorry, there are no movies based on your filter criteria...</p>
                </div>
            );
        }
        //Render movie list as usual...
        return (
            <div id="movie-list" className="fade-in">
                {
                    this.filterList().map(movie => <MovieCard key={movie.id} movie={movie} />)
                }
            </div>
        );

    }

};