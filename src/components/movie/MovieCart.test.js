//---------------------------------------------
// Unit Test packages
//---------------------------------------------
import React from 'react';
import { configure, shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
// https://airbnb.io/enzyme/docs/api/

//---------------------------------------------
// Local packages 
//---------------------------------------------
import MovieCard from './MovieCard';
import Movie from '../../models/Movie';
import ContentService from '../../services/ContentService';
//---------------------------------------------

describe('MovieCard Component', () => {
	configure({ adapter: new Adapter() });

	let props, movie;

	beforeEach(() => {
		ContentService.setGenres([
			{id:1, name:'Drama'},
			{id:2, name:'Action'},
			{id:3, name:'Fantasy'}
		]);
		movie = new Movie({
			id:99,
			name:'Movie',
			title:'Terminator 2',
			genre_ids:[1,2,3],
			popularity:5,
			poster_path:'terminator.jpg'
		});
		props = {key:1,movie:movie};
	});
  
	it('renders as expected', () => {
		const component = mount(<MovieCard {...props} />);
		expect(component.find('[data-id="image"]').props().src).toEqual('https://image.tmdb.org/t/p/w500/terminator.jpg');
		expect(component.find('[data-id="title"]').html()).toContain('Terminator 2');
		expect(component.find('[data-id="genres"]').html()).toContain('Drama, Action, Fantasy');
		expect(component.find('[data-id="rating"]').html()).toContain('popularity 5');
	}); 

});