import React, { Component } from 'react';

import MovieService from '../services/MovieService';
import ContentService from '../services/ContentService';
import Movie from '../models/Movie';
import MovieList from './movie/MovieList';
import Sidebar from './SideBar';

export default class Content extends Component {

    constructor() {
        super();

        this.state = {
            movies: [],
            genres: [],
            update: true, //triggered if current state needs update from external components
            status: 'loading' //current state loading status
        };
    }

    componentDidMount() {
        //Set loading state
        this.setState({ status: 'loading' });

        //Load all movies and genres...
        return Promise.all([MovieService.loadMovies(), MovieService.loadGenres()]).then(values => {
            //Set genres logic...
            let genres = values[1];
            ContentService.setGenres(genres);
            
            //Set movies logic...
            let movies = values[0].map(movie => new Movie(movie));
            
            //set timeout to illustrate loading message...
            setTimeout(function() {
                this.setState({
                    movies: movies, 
                    genres: genres, 
                    status: 'loaded' 
                })
            }.bind(this),1000); 

        })
        .catch(response => this.setState({ status: 'error' }));
    }

    updateState() {
        ContentService.toggleUpdate();
        this.setState({ update: ContentService.getUpdate() });
    }    
    
    render() {
        //Display loading indicator....
        if(this.state.status === 'loading'){
            return (
                <div className="message">
                    <p>Loading Movies, please wait...</p>
                </div>
            );
        //Display error indicator...
        }else if(this.state.status === 'error'){
            return (
                <div className="message">
                    <p>Opps, something went wrong...</p>
                </div>
            );
        }
        //Otherwise, render content...
        return (
            <div className="flex-row">
                <Sidebar movies={this.state.movies} updateState={ this.updateState.bind(this) } />
                <MovieList movies={this.state.movies}  />
            </div>
        );

    }

};