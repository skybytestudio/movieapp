import React, { Component } from 'react';
import ContentService from '../services/ContentService';

// Sidebar
export default class Sidebar extends Component {

    constructor() {
        super();

        this.state = {
			ratingSelected: 3,
			genresSelected: []
		};

		
		//needed to invoke this.setState method...
		this.handleRatingChange = this.handleRatingChange.bind(this); 
		this.handleGenresChange = this.handleGenresChange.bind(this); 
	}
	
	componentDidMount() {
		//filter genres only with names that appear in movies on the screen
		let genresSelected = ContentService.getGenres().filter(genre => {
			genre.selected = false; //set default checkbox state
			return this.props.movies.find(movie => movie.genre_ids.includes(genre.id));
		})
		this.setState({genresSelected: genresSelected});
	}		

	handleRatingChange(event) {
		let value = event.target.value;
		this.setState({ratingSelected: value});
		//----------------------------------
		//Update parent component state
		//----------------------------------
		ContentService.setRating(value); //update filter
		this.props.updateState();
	}

	handleGenresChange = (event, data) => {
		let isSelected = event.target.checked;
		this.state.genresSelected
			.filter(genre => genre.id === data.id)
			.map(genre => genre.selected = isSelected )

		//----------------------------------
		//Update parent component state
		//----------------------------------		
		ContentService.setGenres(this.state.genresSelected); //update filter
		this.props.updateState();
	}	

	render() {
		return (
			<div id="sidebar">
				<h4>Rating: {this.state.ratingSelected}</h4>
				<input 
					type="range" 
					min="0" 
					max="10" 
					step="0.5" 
					value={this.state.ratingSelected} 
					onChange={this.handleRatingChange} />

				<h4>Genres</h4>
				<ul id="sidebar-genres">
					{this.state.genresSelected.map(genre => 
						<li key={genre.id}>
							<label htmlFor={genre.id}>
								<input 
									type="checkbox" 
									id={genre.id}
									onChange={((event) => this.handleGenresChange(event, genre))} 
								/> {genre.name}
							</label>
						</li>
					)}
				</ul>
			</div>
		);
	}
};
