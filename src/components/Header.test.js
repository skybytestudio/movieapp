//---------------------------------------------
// Unit Test packages
//---------------------------------------------
import React from 'react';
import { configure, shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
// https://airbnb.io/enzyme/docs/api/

//---------------------------------------------
// Local packages 
//---------------------------------------------
import Header from './Header';
//---------------------------------------------

describe('Header Component', () => {
	configure({ adapter: new Adapter() });

    it('renders as expected', () => {
		const component = mount(<Header />);
		expect(component.find('[id="header"]').html()).toContain('Popular Movies');
	}); 

});