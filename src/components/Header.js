import React, { Component } from 'react';
import logo from '../images/logo.png';

// Header
export default class Header extends Component {
	render() {
		return (
			<div id="header">
				<div className="flex-col">
					<img src={logo} id="logo" alt="logo" />
				</div>
				<div className="flex-col">
					<h1>Popular Movies</h1>
				</div>
			</div>
		);
	}
};
