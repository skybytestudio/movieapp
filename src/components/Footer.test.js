//---------------------------------------------
// Unit Test packages
//---------------------------------------------
import React from 'react';
import { configure, shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
// https://airbnb.io/enzyme/docs/api/

//---------------------------------------------
// Local packages 
//---------------------------------------------
import Footer from './Footer';
//---------------------------------------------

describe('Footer Component', () => {
	configure({ adapter: new Adapter() });

    it('renders as expected', () => {
		const component = mount(<Footer />);
		expect(component.find('[id="footer"]').html()).toContain('Copyright @ 2018');
	}); 

});