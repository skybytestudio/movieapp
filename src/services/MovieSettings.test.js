//---------------------------------------------
// Unit Test packages
//---------------------------------------------
import React from 'react';
import { configure, shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
// https://airbnb.io/enzyme/docs/api/

//---------------------------------------------
// Local packages 
//---------------------------------------------
import MovieSettings from './MovieSettings';
//---------------------------------------------

describe('MovieSettings', () => {
	configure({ adapter: new Adapter() });

    it('returns properties as expected', () => {
        expect(MovieSettings.apiKey).toEqual('9b1b2768961890a85fb7ef85e017eb9b');
        expect(MovieSettings.baseUrl).toEqual('https://api.themoviedb.org/3');
        expect(MovieSettings.imageUrl).toEqual('https://image.tmdb.org/t/p/w500/');
    }); 

    
});