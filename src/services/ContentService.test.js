//---------------------------------------------
// Unit Test packages
//---------------------------------------------
import React from 'react';
import { configure, shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
// https://airbnb.io/enzyme/docs/api/

//---------------------------------------------
// Local packages 
//---------------------------------------------
import ContentService from './ContentService';
//---------------------------------------------

describe('ContentService', () => {
	configure({ adapter: new Adapter() });

    it('init as expected', () => {
        expect(ContentService.update).toEqual(false);
        expect(ContentService.rating).toEqual(3);
        expect(ContentService.genres).toEqual([]);
    }); 

    it('calls toggleUpdate() / getUpdate() as expected', () => {
        ContentService.toggleUpdate();
        expect(ContentService.getUpdate()).toEqual(true);
        ContentService.toggleUpdate();
        expect(ContentService.getUpdate()).toEqual(false);
    }); 

    it('calls getRating() / setRating() as expected', () => {
        ContentService.setRating(5);
        expect(ContentService.getRating()).toEqual(5);
    }); 

    it('calls setGenres() / getGenres() as expected', () => {
        ContentService.setGenres([{id:1}]);
        expect(ContentService.getGenres()).toEqual([{id:1}]);
    }); 
    
});