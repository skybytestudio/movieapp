
export default class ContentService {

    static update = false;
    static rating = 3;
    static genres = [];

    /**
    * Toggle application update status 
    * 
    */    
    static toggleUpdate(){
        this.update = !this.update;
    }

    /**
    * Get application update status
    * 
    * @return {bool} update 
    */    
    static getUpdate(){
        return this.update;
    }

    /**
    * Set filter rating
    * 
    * @param  {bool} rating - true/false
    */    
    static setRating(rating){
        this.rating = rating;
    }
    
    /**
    * Get filter rating
    * 
    * @return {bool} rating 
    */    
    static getRating(){
        return this.rating;
    }

    /**
    * Set filter genres
    * 
    * @param {Array} genres 
    */    
    static setGenres(genres){
        this.genres = genres;
    }

    /**
    * Get filter genres
    * 
    * @return {Array} genres 
    */    
    static getGenres(){
        return this.genres;
    }


}