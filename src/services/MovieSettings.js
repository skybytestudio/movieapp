
export default class MovieSettings {

    /**
    * Get movies DB apiKey
    * @return {string} apiKey 
    */    
    static get apiKey () {
        return '9b1b2768961890a85fb7ef85e017eb9b';
    }

    /**
    * Get movies DB baseUrl
    * @return {string} baseUrl 
    */    
    static get baseUrl() {
        return 'https://api.themoviedb.org/3';
    }    

    /**
    * Get movies DB imageUrl
    * @return {string} imageUrl 
    */    
    static get imageUrl() {
        return 'https://image.tmdb.org/t/p/w500/';
    }    
}
