import MovieSettings from './MovieSettings';

export default class MovieService {

    /**
    * Load list of Movies
    * @return {Array} movies 
    */    
    static loadMovies() {
        return fetch(MovieSettings.baseUrl+'/movie/now_playing?api_key='+MovieSettings.apiKey)
            .then((response) => response.json()).then((data) => {
                return data.results;
            })
            .catch((err) => {
                return [];
            })
    }

    /**
    * Load list of Genres
    * @return {Array} genres 
    */    
    static loadGenres () {
        return fetch(MovieSettings.baseUrl+'/genre/movie/list?api_key='+MovieSettings.apiKey)
            .then((response) => response.json()).then((data) => {
                return data.genres;
            })
            .catch((err) => {
                return [];
            })
    }


}