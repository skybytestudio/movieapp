//---------------------------------------------
// Unit Test packages
//---------------------------------------------
import React from 'react';
import { configure, shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
// https://airbnb.io/enzyme/docs/api/

//---------------------------------------------
// Local packages 
//---------------------------------------------
import MovieService from './MovieService';
//---------------------------------------------

describe('MovieService', () => {
	configure({ adapter: new Adapter() });

    it('calls loadMovies() as expected', () => {
        expect.assertions(1);
        return MovieService.loadMovies().then(data => {
            expect(data.length).toBeGreaterThan(0);
        }).catch(response => expect(response).toEqual([]))
    });
    
    
    it('calls loadGenres() as expected', () => {
        expect.assertions(1);
        return MovieService.loadGenres().then(data => {
            expect(data.length).toBeGreaterThan(0);
        }).catch(response => expect(response).toEqual([]))
    });
    
});